const Elasticerror = require('./elasticerror');
const ElasticBaseQuery = require('./elasticbasequery');

class Elasticquery extends ElasticBaseQuery {
    constructor(o = {}) {
        super(o);
        this._index = o.index;
        this._doctype = o.doctype || o.index;
        this._filter_must = o.filter_must || [];
        this._filter_should = o.filter_should || [];
        this._filter_must_not = o.filter_must_not || [];
        this._sort = o.sort || [];
        this._aggs = o.aggs || {};
        this._start = o.start || 0;
        this._size = o.size || 10;
        this._minimum_should_match = o.minimum_should_match || null;
        this._search_after = o.search_after || null;
        this._server = o.server || null;
        this._query_extend = o.query_extend || {};
        return this;
    }
    static getIndex({ index, server }) {
        return server.globalPrefix + index;
    }
    static async indicesExists({ index, server }) {
        try {
            let rs = await server.indices.exists({ index: Elasticquery.getIndex({ index, server }) });
            if (rs.statusCode != 200)
                return false;
            return rs.body;
        } catch (e)  { Elasticerror.makeError({ server, e }); return false; }
    }
    static async indicesSetup({ server, index, map }) {
        try {
            if (!map)
                return false;
            let rq = await server.indices.create({ index: Elasticquery.getIndex({ index, server }), body: map });
            return (rq.statusCode !== 200) ? false : true;
        } catch (e) { Elasticerror.makeError({ server, e }); return false; }
    }
    static async deleteIndex({ index, server }) {
        try {
            let rq = await server.indices.delete({ index: Elasticquery.getIndex({ index, server }) });
            return (rq.statusCode !== 200) ? false : rq.body.acknowledged;
        } catch (e) { Elasticerror.makeError({ server, e }); return false; }
    }
    static async esBulk({ data, server, refresh = true }) {
        try {
            let rq = await server.bulk({ 'body': data, refresh });
            return rq.body;
        } catch (e) { Elasticerror.makeError({ server, e }); return false; }
    }
    static async msearch({ server, body, index_as_key = true, search_type= 'query_then_fetch', raw=false }) {
        try {
            let rq, data, response, result = [];
            for (let item of body) {
                if (!item.index || item.index === '')
                    continue;
                if (index_as_key)
                    item.raw_index = item.index;
                item.index = Elasticquery.getIndex({ index: item.index, server });
            }
            rq = await server.msearch({ body, search_type });
            if(raw)
                return rq;
            data = rq.body.responses;
            for (let i = 0; i < body.length; i++) {
                response = data[i];
                if (!response || response.status !== 200)
                    continue;
                for (let item of response.hits.hits)
                    result.push((index_as_key) ? {index: body[i].raw_index, data: item._source } : item._source);
            }
            return result;
        } catch (e) { Elasticerror.makeError({ server, e }); return []; }
    }
    async get({ id, server, options = {} }) {
        try {
            let rs = await server.get({ index: Elasticquery.getIndex({ index: this._index, server }), type: this._doctype, id: id });
            return (options.raw) ? rs.body : rs.body._source;
        } catch (e) { Elasticerror.makeError({ server, e }); return null; }
    }
    async search({ server, options = {} }) {
        try {
            let index = Elasticquery.getIndex({ index: this._index, server }),
                rs = [],
                rq;
            rq = await server.search({ index, type: this._doctype, body: this.query_body });
            if (rq.body.hits.hits.length == 0)
                return [];
            if (options.raw)
                return rq.body;
            for (let item of rq.body.hits.hits)
                rs.push(item._source);
            return rs;
        } catch (e) { Elasticerror.makeError({ server, e }); return []; }
    }
    async searchFirst({ server, options = {} }) {
        this.size = 1;
        try {
            let data = await this.search({ server, options });
            if (options.raw)
                data = data.body.hits.hits;
            return (data.length === 0) ? null : data[0];
        } catch (e) { Elasticerror.makeError({ server, e }); return null; }
    }
    async exists({ id, server }) {
        try {
            let rq = await server.exists({ index: Elasticquery.getIndex({ index: this._index, server }), type: this._doctype, id: id });
            return (rq.statusCode !== 200) ? false : true;
        } catch (e) { Elasticerror.makeError({ server, e }); return false; }
    }
    async count({ server }) {
        try {
            let rq = await server.count({ index: Elasticquery.getIndex({ index: this._index, server }), body: { query: this.query_body.query } });
            return rq.body.count;
        } catch (e) { Elasticerror.makeError({ server, e }); return 0; }
    }
    async aggregate({ server, options = {} }) {
        try {
            let rq = await server.search({ index: Elasticquery.getIndex({ index: this._index, server }), type: this._doctype, body: this.query_body });
            return rq.body.aggregations;
        } catch (e) { Elasticerror.makeError({ server, e }); return false; }
    }
    async update({ id, data, server, options = {}, refresh = true, retry_on_conflict = 5 }) {
        try {
            let rq = await server.update({ index: Elasticquery.getIndex({ index: this._index, server }), type: this._doctype, id: id, body: { doc: data }, refresh, retry_on_conflict });
            return (rq.statusCode != 200) ? false : true;
        } catch (e) { Elasticerror.makeError({ server, e }); return false; }
    }
    async create({ id, data, server, options = {}, refresh = true }) {
        try {
            let rq = await server.create({ index: Elasticquery.getIndex({ index: this._index, server }), type: this._doctype, id: id, body: data, refresh });
            return (rq.statusCode != 201) ? false : true;
        } catch (e) { Elasticerror.makeError({ server, e }); return false; }
    }
    async set({ id, data, server, options = {}, refresh = true }) {
        try {
            return (await this.exists({ id, server })) ? await this.update({ id, data, server, options, refresh, retry_on_conflict: 3 }) : await this.create({ id, data, server, options, refresh });
        } catch (e) { Elasticerror.makeError({ server, e }); return false; }
    }
    async delete({ id, server, refresh = true }) {
        try {
            let rq = await server.delete({ index: Elasticquery.getIndex({ index: this._index, server }), type: this._doctype, id, refresh });
            return (rq.statusCode != 200) ? false : true;
        } catch (e) { Elasticerror.makeError({ server, e }); return false; }
    }
}


module.exports = Elasticquery;