class Elasticerror {
    static makeError({server, e}){
        if (!server || !server.globalError)
            return;
        console.error(JSON.stringify(e));
    }
}

module.exports = Elasticerror;