module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es6: true
    },
    extends: 'eslint:recommended',
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly'
    },
    parserOptions: {
        ecmaVersion: 2018
    },
    rules: {
        'no-console': 1,
        'semi': [2, 'always'],
        'quotes': [2, 'single'],
        'new-cap': [2, { newIsCap: true, capIsNew: false }],
        'no-extra-boolean-cast': 2,
    },
};