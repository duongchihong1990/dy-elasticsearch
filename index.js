const _e = require('./src/core/elasticquery');
exports.ElasticqueryStatic = _e;
exports.ElasticBaseQuery = require('./src/core/elasticbasequery');
exports.ElasticConfig = require('./src/core/elasticconfig');
exports.Elasticquery = (...args) => { return new _e(...args); };
